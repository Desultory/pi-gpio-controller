<?php
class GPIO {
	private $ip = '';
	private $pins = [];
	private $pinstate = [];
	//All Valid GPIO Pins
	private $valid_pins = [0, 1, 2, 3, 4, 5, 6, 7, 21, 22, 23, 24, 25, 26, 27, 28, 29];
	
	public function __construct($ip, $pins) {
		$this->ip = $ip;
		$buffer = [];
		foreach($pins as $p) {
			if(in_array($p, $this->valid_pins)) {
				array_push($buffer, $p);
			}
		}
		$this->pins = $buffer;
	}
	
	private function perform_curl($url) {
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_POST, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_TIMEOUT_MS, 1000);
		$response = trim(curl_exec($ch));
		curl_close($ch);
		return $response;
	}

	private function multicurl($requests) {
		$mh = curl_multi_init();
		foreach($requests as $element=>$request) {
			$multiCurl[$element] = curl_init();
			curl_setopt($multiCurl[$element], CURLOPT_URL, $request);
			curl_setopt($multiCurl[$element], CURLOPT_FRESH_CONNECT, true);
			curl_setopt($multiCurl[$element], CURLOPT_HEADER, 0);
			curl_setopt($multiCurl[$element], CURLOPT_RETURNTRANSFER, 1);
			curl_multi_add_handle($mh, $multiCurl[$element]);
		}
		$active = null;
		do {
			$mrc = curl_multi_exec($mh, $active);
		} while($mrc === CURLM_CALL_MULTI_PERFORM);
		while($active && $mrc === CURLM_OK) {
			if( curl_multi_select($mh) !== -1 ) {
				do {
					curl_multi_exec($mh, $active);
				} while ($mrc === CURLM_CALL_MULTI_PERFORM);
			}
		}
		foreach($multiCurl as $element => $ch) {
			$result[$element] = curl_multi_getcontent($ch);
			curl_multi_remove_handle($mh, $ch);
		}
		curl_multi_close($mh);
		return $result;
	}

	private function get_gpio_state($pin) {
		$ip = $this->ip;
		$request = "http://$ip/gpio.php?p=$pin&r=1";
		$response = $this->perform_curl($request);
		return $response;
	}
	
	private function pin_on($pin) {
		$ip = $this->ip;
		$request = "http://$ip/gpio.php?p=$pin&w=1&s=1";
		$this->perform_curl($request);
	}

	private function pin_off($pin) {
		$ip = $this->ip;
		$request = "http://$ip/gpio.php?p=$pin&w=1&s=0";
		$this->perform_curl($request);
	}

	public function blink($delay, $count) {
		$ip = $this->ip;
		$pins = $this->pins;
		$this->save_state();
		$pinstate = $this->pinstate;
		
		for($i = 0; $i < $count; $i++) {
			foreach($pinstate as $pin => $state) {
				if($state) {
					$requests[$pin] = "http://$ip/gpio.php?p=$pin&w=1&s=0";
					$pinstate[$pin] = 0;
				} else {
					$requests[$pin] = "http://$ip/gpio.php?p=$pin&w=1&s=1";
					$pinstate[$pin] = 1;
				}
			}
			$this->multicurl($requests);
			usleep($delay);
		}
	}

	public function light_increment() {
		$ip = $this->ip;
		$pins = $this->pins;
		foreach($pins as $pin) {
			if( !$this->get_gpio_state($pin)) {
				
				$request = "http://$ip/gpio.php?p=$pin&w=1&s=1";
				$this->perform_curl($request);
				return 1;
			}
		}
	}

	public function light_decrement() {
		$ip = $this->ip;
		$pins = $this->pins;
		foreach($pins as $pin) {
			if($this->get_gpio_state($pin)) {
				$request = "http://$ip/gpio.php?p=$pin&w=1&s=0";
				$this->perform_curl($request);
				return 1;
			}
		}
	}

	public function lights_on() {
		$ip = $this->ip;
		$pins = $this->pins;
		foreach($pins as $element => $pin) {
			$request = "http://$ip/gpio.php?p=$pin&w=q&s=1";
			$requests[$element] = $request;
		}
		$this->multicurl($requests);
	}

	public function lights_off() {
		$ip = $this->ip;
		$pins = $this->pins;
		foreach($pins as $element => $pin) {
			$request = "http://$ip/gpio.php?p=$pin&w=1&s=0";
			$requests[$element] = $request;
		}
		$this->multicurl($requests);
	}

	public function ordered_blink() {
		$pins = $this->pins;
		foreach($pins as $pin) {
			$this->pin_on($pin);
			$this->pin_off($pin);
		}
	}

	public function cascade() {
		$pins = $this->pins;
		foreach($pins as $pin) {
			$this->pin_on($pin);
		}
		foreach($pins as $pin) {
			$this->pin_off($pin);
		}
	}

	public function save_state() {
		$ip = $this->ip;
		$pins = $this->pins;
		foreach($pins as $pin) {
			$requests[$pin] = "http://$ip/gpio.php?p=$pin&r=1";
		}
		$state = $this->multiCurl($requests);
		$this->pinstate = $state;
	}

	public function restore_state() {
		$ip = $this->ip;
		$pinstate = $this->pinstate;
		foreach($pinstate as $pin => $state) {
			$requests[$pin] = "http://$ip/gpio.php?p=$pin&w=1&s=$state";
		}
		$this->multicurl($requests);
	}
}