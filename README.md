Simply make a webserver supporting php on a raspberry pi and place "gpio.php" in the root folder
Then put the "GPIO_Controller.php" on another webserver (or the same one).

Look at lightson.php and lightsoff.php for examples on how to make scripts for the pins.

If gpio.php is somewhere other than the webroot, just append the location in the "ip" argument when making GPIO objects.

I suggest putting the gpio.php on another device because it is inherently insecure which is why I use a webserver as a proxy for that script.

I made this to control lights which are connected to relays on a rpi zero w which is why a lot of the terminology used in this code is based around lights.
